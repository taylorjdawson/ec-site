const constants = {
  cdnBaseUrl: 'https://res.cloudinary.com/tdawson/image/upload',
  dirName: 'ec-site',
  designImgs: [
    'ros_social_media',
    'lam',
    'mycology',
    'brolly',
    'fi',
    'winteraid',
    'pink',
    'blue',
    'o',
    'gold',
    'white',
    'grey',
    'two_redials_horizontal',
    'black_spotify',
    'red',
    'creme',
    'orange',
    'green',
    '666'
  ],
  artImgs: [
    'copy',
    'this_is_it',
    'skyswitch',
    'city',
    'sweetness_and_danger',
    'fairytales_and_firesides',
    'blind',
    'Untitled-6',
    'Lately',
    'reset'
  ]
}

export default {
  menuOpen: false,
  constants
}
