export default {
  toggleMenu({ commit, state }) {
    commit('SET_MENU_OPEN', !state.menuOpen)
    // document.documentElement.style.overflow = state.menuOpen ? 'hidden' : 'auto';
  }
}
