export default {
  SET_MENU_OPEN(state, bool) {
    state.menuOpen = bool
  }
}
