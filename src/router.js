import Vue from 'vue'
import Router from 'vue-router'
import Art from './views/Art.vue'
import About from './views/About.vue'
import Current from './views/Current.vue'
import Design from './views/Design.vue'
import Thinks from './views/Thinks.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'art',
      component: Art
    },
    {
      path: '/art',
      name: 'art',
      component: Art
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/current',
      name: 'current',
      component: Current
    },
    {
      path: '/design',
      name: 'design',
      component: Design
    },
    {
      path: '/thinks',
      name: 'thinks',
      component: Thinks
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
