export default {
  name: 'resizable-textarea',
  methods: {
    resizeTextarea(event) {
      event.target.style.height = 'auto'
      event.target.style.height = `${event.target.scrollHeight - 17}px`
    }
  },
  mounted() {
    this.$nextTick(() => {
      this.$el.setAttribute('style', `height:${this.$el.scrollHeight - 17}px;overflow-y:hidden;`)
    })

    this.$el.addEventListener('input', this.resizeTextarea)
  },
  beforeDestroy() {
    this.$el.removeEventListener('input', this.resizeTextarea)
  },
  render() {
    return this.$slots.default[0]
  }
}
