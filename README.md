# ec-site

### Ratios: 
Image sizes are super important here. These are some notes for development.

Chrome window width should be: 1200px<br/>
The design spec width: 1920px

Note:
Vertical spacing using `vw` holds positioning the best when resizing.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
